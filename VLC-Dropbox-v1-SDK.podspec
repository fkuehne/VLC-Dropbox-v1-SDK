Pod::Spec.new do |s|

  s.name         = "VLC-Dropbox-v1-SDK"
  s.version      = "1.3.14w"
  s.summary      = "The Dropbox v1 SDK for iOS and tvOS"

  s.description  = <<-DESC
  A fork of the v1 Dropbox SDK to add basic playback support on tvOS.
                   DESC

  s.homepage     = "https://code.videolan.org/fkuehne/VLC-Dropbox-v1-SDK"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.authors            = { "2010-2014 Dropbox (Evenflow, Inc.)" => "api-support@dropbox.com", "Felix Paul Kühne" => "fkuehne@videolan.org" }

  s.ios.deployment_target = "7.0"
  s.tvos.deployment_target = "9.0"

  s.source       = { :git => "https://code.videolan.org/fkuehne/VLC-Dropbox-v1-SDK.git", :tag => "#{s.version}" }
  s.public_header_files = "DropboxSDK/Classes/DropboxSDK/*.h"

  s.prefix_header_file = "DropboxSDK/Classes/DropboxSDK_Prefix.pch"
  s.source_files  = "DropboxSDK/Classes/**/*.{h,m}", "DropboxSDK/Classes/**/*.{h,c}"
  s.exclude_files = "DropboxSDK/Classes/DBKeychain-OSX.m", "DropboxSDK/Classes/DBRestClient+OSX.m", "DropboxSDK/Classes/DBAuthHelperOSX.m", "DropboxSDK/Classes/DropboxSDK/MPOAuth.h"
  s.tvos.exclude_files ="DropboxSDK/Classes/DBConnectController.m"

  s.ios.header_dir = "DropboxSDK"
  s.tvos.header_dir = "DropboxTVSDK"

  s.frameworks  = "Foundation", "UIKit"
  
  s.requires_arc = false

end
